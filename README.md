Server-side technologies
    Spring Boot 
    JDK - 1.8
    Spring Framework 
    Hibernate 
    Spring Data JPA 

Tools
    Maven
    IDE - Spring Tool Suite (STS) 

End Points
    
    GET:
    http://localhost:8080/empleados

    Muestra la lista de todos los empleados.
    
    {
    "_embedded": {
        "empleadoses": [
            {
                "id": 1,
                "nombre": "Juan1",
                "aPaterno": "Luis1",
                "aMaterno": "xxxxx1",
                "edad": 21,
                "_links": {
                    "self": {
                        "href": "http://localhost:8080/empleados/1"
                    },
                    "empleados": {
                        "href": "http://localhost:8080/empleados/1"
                    }
                }
            },

    GET:
    http://localhost:8080/empleados/3
    
    MUESTRA UN EMPLEAD EN ESPECIFICO
    
    {
        "id": 3,
        "nombre": "Juan3",
        "aPaterno": "Luis3",
        "aMaterno": "xxxxx3",
        "edad": 23,
        "_links": {
            "self": {
                "href": "http://localhost:8080/empleados/3"
            },
            "empleados": {
                "href": "http://localhost:8080/empleados/3"
            }
        }
    }
    
    POST:
    http://localhost:8080/empleados
    
    INSERTA UN NUEVO EMPLEADO
    
    {
               
        "nombre": "Juan6",
        "aPaterno": "Luis6",
        "aMaterno": "xxxxx6",
        "edad": 26
               
    }


    PUT: 
    http://localhost:8080/empleados/6

    EDITAMOS EL EMPLEADO 6

    {
        "nombre": "Juan6",
        "aPaterno": "Luis6",
        "aMaterno": "xxxxx6",
        "edad": 25
    }


    DELETE:
    http://localhost:8080/empleados/6

    ELIMINAMOS EL EMPLEADO 6
