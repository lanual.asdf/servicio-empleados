package com.sprigboot.app.empleados.models.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.sprigboot.app.empleados.models.entity.Empleados;

@RepositoryRestResource(path="empleados")
public abstract interface EmpleadoDao extends PagingAndSortingRepository<Empleados, Integer>{
	
}
